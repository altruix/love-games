# Einfache Spiele mit dem LÖVE-Framework

Ursprünglich habe ich diese Spiele [hier](https://simplegametutorials.github.io/) gefunden. 

* Verzeichnis `bird`: Originalspiel ist [hier](https://simplegametutorials.github.io/bird/) zu finden. 
* Verzeichnis `blocks`: Tetris-ähnliches Spiel. [Details]( https://simplegametutorials.github.io/blocks/)
* Verzeichnis `eyes`: Kein richtiges Spiel, sondern ein Paar Augen, welche den Mauszeiger "beobachten". [Details](https://simplegametutorials.github.io/eyes/)
* Verzeichnis `snake`: Schlangenspiel. [Details](https://simplegametutorials.github.io/snake/) 
* Verzeichnis `platformer`: Ein sehr einfacher Platformer -- mit den A und D-Tasten kann man sich nach links und rechts bewegen, mit der Leertaste kann man springen. [Tutorial](https://love2d.org/wiki/Tutorial:Baseline_2D_Platformer) 

Du kannst mehrere andere Spiele [hier](https://simplegametutorials.github.io/) finden. 

## Nützliche Links

* [Tutorials](https://love2d.org/wiki/Category:Tutorials) 
* [ZeroBrane](https://studio.zerobrane.com/): IDE für LÖVE 
* [Löve framework](https://love2d.org/) 

## Kommerzielle Spiele

Einige der mit Löve entwickelten Spiele werden auf Steam verkauft:

* [Warlock's Tower](http://store.steampowered.com/app/530370)
* [Metanet Hunter CD](http://store.steampowered.com/app/586570/Metanet_Hunter_CD/) 
* [Move or Die](http://store.steampowered.com/app/323850/Move_or_Die/) 

## Mario

* Dieses [Spiel]( http://stabyourself.net/mari0/) ist eine Umsetzung des Mario-Spiels auf einer älteren Version von LÖVE. Läuft auf meinem Rechner, auch der Quellcode ist vorhanden, aber mit der aktuellen Version von Löve funktioniert das Debuggen nicht. Wenn es Dich interessiert, wie dieses Spiel intern aufgebaut ist, kann ich versuchen, es so aufzusetzen, dass dort das Debuggen möglich wird.
